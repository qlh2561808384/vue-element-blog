export default {
    state: {
        isCollapse: false,
        currentMenu: null,
        menu:[],
        //面包屑
        tabsList: [
            {
                path: '/',
                name: 'home',
                label: '首页',
                icon: 'home'
            }
        ]
    },
    mutations: {
        setMenu(state, val) {
            //vuex添加
            state.menu = val
        },
        clearMenu(state) {
            //清除也一样 vuex和cookie都清除
            state.menu = []
        },
        addMenu(state, router) {
            //添加动态路由 主路由为Main.vue
            let currentMenu = [
                {
                    path: '/',
                    // name:'home',
                    component: () => import(`@/views/Main`),
                    children: []
                }
            ]
            console.log(state.menu);
            //如果是一级菜单 那么菜单名称肯定有路由 如果是二级菜单那么一级没有 二级有路由
            state.menu.forEach(item => {
                console.log(item.children);
                if (item.children) {
                    console.log(item)
                    item.children = item.children.map(item => {
                        item.component = () => import(`@/views/${item.url}`)
                        return item
                    })
                    currentMenu[0].children.push(...item.children)
                } else {
                    item.component = () => import(`@/views/${item.url}`)
                    currentMenu[0].children.push(item)
                }
            })
            //添加动态路由
            router.addRoutes(currentMenu)
        },
        //选择标签 选择面包屑
        selectMenu(state, val) {
            if (val.name !== 'home') {
                state.currentMenu = val
                let result = state.tabsList.findIndex(item => item.name === val.name)
                result === -1 ? state.tabsList.push(val) : ''
                window.localStorage.setItem('tagList', JSON.stringify(state.tabsList))
            } else {
                state.currentMenu = null
            }
            // val.name === 'home' ? (state.currentMenu = null) : (state.currentMenu = val)
        },
        //获取标签
        getMenu(state) {
            if (window.localStorage.getItem('tagList')) {
                let tagList = JSON.parse(localStorage.getItem('tagList'))
                state.tabsList = tagList
            }
        },
        //关闭标签
        closeTab(state, val) {
            let result = state.tabsList.findIndex(item => item.name === val.name)
            state.tabsList.splice(result, 1)
            window.localStorage.setItem('tagList', JSON.stringify(state.tabsList))
        },
        //左侧栏是否水平展开
        collapseMenu(state) {
            state.isCollapse = !state.isCollapse
        }
    }
}
