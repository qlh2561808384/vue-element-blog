import Vue from 'vue'
import Vuex from 'vuex'
import tab from './tab'
import {getTimeInterval} from '../utils/index'
import request from "../http/request";
Vue.use(Vuex)
// 略:后台获取系统运行时间
// const runAt = '1589878800000';
const runAt = '1589878800000';
let timer = null;
const state = {
    loading: false,
    isCollapse: false,
    currentMenu: null,
    menu: [],
    runTimeInterval: '',
    socials: '',
    websiteInfo: ''
};
const mutations = {
    setMenu(state, val) {
        //vuex添加
        state.menu = val
    },
    clearMenu(state) {
        //清除也一样 vuex和cookie都清除
        state.menu = []
    },
    addMenu(state, router) {
        //添加动态路由 主路由为Main.vue
        let currentMenu = [
            {
                path: '/',
                // name:'home',
                component: () => import(`@/views/Main`),
                children: []
            }
        ]
        console.log(state.menu);
        //如果是一级菜单 那么菜单名称肯定有路由 如果是二级菜单那么一级没有 二级有路由
        state.menu.forEach(item => {
            console.log(item.children);
            if (item.children) {
                console.log(item)
                item.children = item.children.map(item => {
                    item.component = () => import(`@/views/${item.url}`)
                    return item
                })
                currentMenu[0].children.push(...item.children)
            } else {
                item.component = () => import(`@/views/${item.url}`)
                currentMenu[0].children.push(item)
            }
        })
        //添加动态路由
        router.addRoutes(currentMenu)
    },
    //左侧栏是否水平展开
    collapseMenu(state) {
        state.isCollapse = !state.isCollapse
    },
    GET_RUNTIME_INTERVAL: (state) => {
        if (!timer || !state.runTimeInterval) {
            clearInterval(timer)
            timer = setInterval(() => {
                state.runTimeInterval = getTimeInterval(runAt);
            }, 1000);
        }
    },
    SET_LOADING: (state, v) => {
        state.loading = v;
    },
    SET_SOCIALS: (state, v) => {
        state.socials = v;
        console.log(state.socials)
    },
    SET_SITE_INFO: (state, v) =>{
        state.websiteInfo = v;
    },
};
const actions = {
    initComputeTime: ({commit}) => {
        commit('GET_RUNTIME_INTERVAL');
    },
    setLoading: ({commit}, v) => {
        commit('SET_LOADING', v);
    },
    getSocials: ({commit, state}) => {
        return new Promise((resolve, reject) => {
            if (state.websiteInfo){
                resolve(state.websiteInfo)
            }else {
                request.mock.fetchSocial().then((data) => {
                    commit('SET_SOCIALS', data.data.data);
                    resolve(data.data.data);
                }).catch((err)=>{
                    resolve({});
                });
            }
        })
    },
    getSiteInfo: ({commit, state}) => {
       return new Promise((resolve, reject) => {
            if (state.websiteInfo){
                resolve(state.websiteInfo)
            }else {
                request.mock.fetchSiteInfo().then((data) => {
                    commit('SET_SITE_INFO', data.data.data);
                    resolve(data.data.data);
                }).catch((err)=>{
                    resolve({});
                });
            }
        })
    }
};
const getters = {
    loading: state => state.loading,
    runTimeInterval: state => state.runTimeInterval,
    notice: state => state.websiteInfo ? state.websiteInfo.notice : ''
};
export default new Vuex.Store({
    state,
    mutations,
    actions,
    modules: {},
    getters
})
