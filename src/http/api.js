/*
  统一管理存放后端提供接口
*/
import Goods from './api/goods.js';

const GLOBAL_API_URL = "http://localhost:8088";
export default {
  // 首页
  Index: {
    index: GLOBAL_API_URL + '/index/index'
  },

  // 个人中心
  Home: {
    UserInfo: GLOBAL_API_URL + '/user/info'
  },

  User: {
    register: GLOBAL_API_URL + '/user/register',
    admin: GLOBAL_API_URL + '/user/admin',
    login: GLOBAL_API_URL + '/login',
    getPasswordByEmail: GLOBAL_API_URL + '/user/getPasswordByEmail',
    wxPay: GLOBAL_API_URL + '/order/WxPay',
    aliPay: GLOBAL_API_URL + '/order/AliPay'
    // wxPay: GLOBAL_API_URL + '/api/v1/order/buy'
  },
  // 当然也可以用文件方式进行管理
  Goods: Goods,
  mock:{
    fetchSiteInfo: '/site',
    fetchSocial: '/social',
    fetchFocus: '/focus/list',
    fetchList: '/post/list'
  }

};
