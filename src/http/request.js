import http from './http';
import api from './api';
import el from "element-ui/src/locale/lang/el";

let auth = true;

export default {
  register(data) {
    return http.post(api.User.register, JSON.stringify(data), auth);
  },
  admin(data) {
    return http.post(api.User.admin, JSON.stringify(data), auth);
  },
  login(data) {
    return http.post(api.User.login, data, auth);
  },
  getPasswordByEmail(data) {
    return http.post(api.User.getPasswordByEmail, data);
  },
  Pay(order,payType) {
    if ('AliPay' === payType) {
      return http.post(api.User.aliPay, order, auth);
    }else {
      return http.post(api.User.wxPay, order, auth);
    }
  },
  aliPay(amount,Pay) {
    return http.get("http://localhost:8888/aliPay/toPay/" + amount + '/' + Pay);
  },
  mock:{
    fetchSiteInfo() {
      return http.get(api.mock.fetchSiteInfo, auth);
    },
    fetchSocial(){
      return http.get(api.mock.fetchSocial, auth);
    },
    fetchFocus() {
      return http.get(api.mock.fetchFocus, auth);
    },
    fetchList(){
      return http.get(api.mock.fetchList, auth);
    }
  }
}
