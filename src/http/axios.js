/*
    用户做 axios 拦截器
*/
import axios from 'axios';
import api from './api'
import router from '../router';

// 请求拦截
axios.interceptors.request.use(config => {
    // 1. 这个位置就请求前最后的配置
    // 2. 当然你也可以在这个位置 加入你的后端需要的用户授权信息
    let token = window.localStorage.getItem('token');
    if (config.url === api.User.register || config.url === api.User.getPasswordByEmail) {
    } else {
        config.headers.Authorization = token;
    }
    console.log(config);
    return config
}, error => {
    return Promise.reject(error)
})

// 响应拦截
axios.interceptors.response.use(response => {
    // 请求成功
    // 1. 根据自己项目需求定制自己的拦截
    // 2. 然后返回数据

    //如果token值发生改变的时候，替换token值
    if (response.headers.Authorization) {
        window.localStorage.setItem("token", response.headers.Authorization);
    }
    console.log(response);
    // 对响应数据做点什么
    return response;
}, error => {
    // 请求失败
    if (error && error.response) {
        switch (error.response.status) {
            case 400:
                // 对400错误您的处理\
                Toast({
                    message: '参数错误',
                    duration: 1500,
                    forbidClick: true
                });
                // localStorage.removeItem('token');
                // store.commit('loginSuccess', null);
                // 跳转登录页面，并将要浏览的页面fullPath传过去，登录成功后跳转需要访问的页面
                setTimeout(() => {
                    router.replace({
                        path: '/',
                        query: {
                            redirect: router.currentRoute.fullPath
                        }
                    });
                }, 1000);
                break
            // 在登录成功后返回当前页面，这一步需要在登录页操作。
            // 401 token过期
            // 登录过期对用户进行提示
            // 清除本地token和清空sessionStorage的
            // // 跳转登录页面
            case 401:
                // 清除token
                localStorage.removeItem('token');
                // this.$message.error="token已过期";
                // store.commit('loginSuccess', null);
                // 跳转登录页面，并将要浏览的页面fullPath传过去，登录成功后跳转需要访问的页面
                router.replace({
                    path: '/login',
                    query: {
                        redirect: router.currentRoute.fullPath
                    }
                });
                this.$message.error("登入已经过期")

                break;
            // 404请求不存在                /*  */
            case 403:
                Toast({
                    message: '没有当前操作的权限',
                    duration: 1500,
                    forbidClick: true
                });

                // 跳转登录页面，并将要浏览的页面fullPath传过去，登录成功后跳转需要访问的页面
                setTimeout(() => {
                    router.replace({
                        path: '/',
                        query: {
                            redirect: router.currentRoute.fullPath
                        }
                    });
                }, 1000);
                break;
            default:
                // 如果以上都不是的处理
                return Promise.reject(error);
        }
    }
})
export default axios;
