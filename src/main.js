import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './assets/css/style.less'
import './assets/font/iconfont.css'
import ElementUI from "element-ui"; //加载element-ui前端ui框架
import "element-ui/lib/theme-chalk/index.css"; //加载element-ui前端ui框架样式
import './http/axios'// axios 拦截器
import './mock'//添加mock才能把接口暴漏出来 axios才能拦截请求
Vue.use(ElementUI);
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
/*
devDependencies中的插件只用于开发环境，不用于生产环境，而dependencies是需要发布到生产环境的。
像gulp、babel、webpack这些压缩代码、打包的工具，在实际运行中不需要，所以用-D；
像elementui、echarts这些插件在实际运行中也是需要的，所以用-S。
*/
