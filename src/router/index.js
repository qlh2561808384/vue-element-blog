import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '../store'
Vue.use(VueRouter)

const routes = [
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})
router.beforeEach((to, from, next) => {
  if (to.path !== from.path) {
    store.dispatch('setLoading', true);
  }
  next();
})
router.afterEach((to, from) => {
  // 最多延迟 关闭 loading
  setTimeout(() => {
    store.dispatch('setLoading', false);
  }, 1500)
})
export default router
