import request from "../../../http/request";
import da from "element-ui/src/locale/lang/da";
function forgetPassword() {
  this.$prompt(
      "请输入邮箱，你会收到一封附带你账号密码的电子邮件",
      "密码找回", {
        confirmButtonText: "确定",
        cancelButtonText: "取消",
        inputPattern: /[\w!#$%&'*+/=?^_`{|}~-]+(?:\.[\w!#$%&'*+/=?^_`{|}~-]+)*@(?:[\w](?:[\w-]*[\w])?\.)+[\w](?:[\w-]*[\w])?/,
        inputErrorMessage: "邮箱格式不正确",
        roundButton: true,
        center: true
      }
    )
    .then(({
      value
    }) => {
      this.$message({
        type: "success",
        message: "你的邮箱是: " + value
      });
        request.getPasswordByEmail({email: value}).then((data) => {
            console.log(data);
        });
    })
    .catch(() => {
      this.$message({
        type: "info",
        message: "取消输入"
      });
    });
}

let code = () => {
  alert(0);
};
export {
  forgetPassword,
  code
}
