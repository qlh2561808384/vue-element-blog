const Mock = require('mockjs');
export default {
    fetchSocial:()=>{
        return {
            code: 20000,
            data: [
                {
                    id: 1,
                    title: 'QQ',
                    icon: 'iconqq',
                    color: '#1AB6FF ',
                    href: 'http://wpa.qq.com/msgrd?v=3&uin=2561808384&site=qq&menu=yes'
                },
                {
                    id: 2,
                    title: 'Gitee',
                    icon: 'icongitee',
                    color: '#d81e06',
                    href: 'https://gitee.com/LongGroup'
                },
                {
                    id: 3,
                    title: 'GitHub',
                    icon: 'icongithub',
                    color: '',
                    href: 'https://github.com/qlh2561808384'
                },
                {
                    id: 4,
                    title: 'CSDN',
                    icon: 'iconcsdn',
                    color: 'red',
                    href: 'https://blog.csdn.net/qlh2561808384'
                }
            ]
        }
    },
    fetchSiteInfo: () => {
        return {
            code: 20000,
            data: {
                avatar: 'https://s2.ax1x.com/2020/01/17/1SCadg.png',
                slogan: 'The way up is not crowded, and most chose ease.',
                name: 'FZY′blog',
                domain: 'https://www.fengziy.cn',
                notice: '本博客的Demo数据由Mockjs生成',
                desc: '一个It技术的探索者'
            }
        }
    }
}