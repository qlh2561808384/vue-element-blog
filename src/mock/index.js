import Mock from 'mockjs'
import site from "./site";
import focus from "./focus";


// 设置200-2000毫秒延时请求数据
// Mock.setup({
//   timeout: '200-2000'
// })

// 首页相关
// 拦截的是 /home/getData
Mock.mock('/social', 'get', site.fetchSocial())
Mock.mock('/site', 'get', site.fetchSiteInfo());
Mock.mock('/focus/list', 'get', focus.fetchFocus());
// Mock.mock(/\/home\/social/, 'get', site.fetchSocial());
// Mock.mock(/\/home\/site/, 'get', site.fetchSiteInfo());